# react-native-lawrence

#### 介绍
react-native脚手架

#### 软件架构
软件架构说明


#### 安装教程

1. react-native init 你的项目名称 --template lawrence
2. react-native link
3. react-native run-android / react-native run-ios

#### 使用说明

1. react-native init 你的项目名称 --template lawrence
2. 目录说明
* **api** 网络请求
* **assets** 资源文件
* **components** 自定义组件
* **config** 静态资源
* **manager** App管理器（包括页面，路由，app状态等）
* **page** 页面，每个页面为一个文件件，其中包括viewModel，page，model，style.js
* **store** 持久化存储
* **utils** 工具类
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
